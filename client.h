//
// Created by ivar on 2021-05-12.
//

#ifndef SKYLORDS_CHAT_TEST_CLIENT_H
#define SKYLORDS_CHAT_TEST_CLIENT_H

#include <boost/asio.hpp>
#include <shared_mutex>

using boost::asio::ip::tcp;
using std::string;
using std::shared_mutex;

struct c_args {
    string ip;
    int port;
};

/*
 * Class containing all client-side functionality. Each client runs on its own thread with no
 * shared data between them (see main function for details). Furthermore, each client spawns
 * its own child thread to run the io event loop asynchronously. All client config is done in
 * the main function.
 */
class Client {
private:
    boost::asio::io_context io;
    tcp::socket sock;
    string ip;
    int port;
    enum { max_len = 1024 };
    char buf[max_len];
    string id;

    /*
     * Entrypoint for read handler loop. Uses async_read (will wait until buffer is full).
     */
    void do_read();

    /*
     * Handler called from do_read. Parses the incoming data and acts appropriately.
     */
    void cont_read(const boost::system::error_code& err, size_t bytes_transferred);

    /*
     * Runs a series of tests cases against the server and then exits.
     */
    void run_test(std::string data);

    /*
     * Infinite loop calling io_context.run()
     */
    [[noreturn]] void io_run();
public:
    explicit Client(c_args &args)
    : sock(tcp::socket(io))
    {
        ip = args.ip;
        port = args.port;
    }
    /*
     * Main function each client thread is run with. Starts the io loop thread as well.
     */
    void run(c_args& args);
};

#endif //SKYLORDS_CHAT_TEST_CLIENT_H
