//
// Created by ivar on 2021-05-12.
//

#include "ConHandler.h"
#include "packets.h"
#include "server.h"
#include <iostream>

using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;
using std::string;
typedef boost::shared_ptr<ConHandler> pointer;

pointer ConHandler::create(boost::asio::io_context& io) {
    return pointer(new ConHandler(io));
}

tcp::socket& ConHandler::socket() {
    return sock;
}

void ConHandler::setup(boost::shared_ptr<Server> s) {
    server = s;
}

void ConHandler::start() {
    do_read();
}

void ConHandler::do_read() {
    auto self = shared_from_this();
    sock.async_read_some(
            // Read into data
            boost::asio::buffer(data, max_length),
            // Lambda for calling handle_read
            strand.wrap([this, self](boost::system::error_code err, std::size_t bytes_transferred)
            { handle_read(err, bytes_transferred); }));
}

// Param is copied to ensure pointer is safe to delete in calling function
void ConHandler::do_write(string w_data) {
    auto self = shared_from_this();
    sock.async_write_some(
            // Write w_data
            boost::asio::buffer(w_data, max_length),
            // Lambda for calling handle_write
            strand.wrap([this, self](boost::system::error_code err, std::size_t bytes_transferred)
            { handle_write(err, bytes_transferred); }));
}

void ConHandler::send_message(MsgOutPacket* msg) {
    do_write(msg->encode());
}

void ConHandler::handle_read(const boost::system::error_code& err, size_t bytes_transferred) {
    if (bytes_transferred == 0) {
        do_read();
        return;
    }

    if (!err) {
        // Handle different types of packets
        std::unordered_map<string, string>  parsed_data = Packet::decode(boost::to_string(data));
        //TODO: Graceful handling of invalid data
        string type = parsed_data["type"];
        if (type == "conn") {
            string name = parsed_data["username"];
            auto *res = new ConnPacket(name);
            server->register_user(this, res);
            delete res;
            do_read();
        }
        else if (type == "msgin") {
            string msg = parsed_data["message"];
            string id = parsed_data["uuid"];
            auto* res = new MsgInPacket(msg, id);
            server->distribute_msg(res);
            delete res;
            do_read();
        }
        else {
            std::cerr << "Bad packet format" << std::endl;
            sock.close();
        }
    }
    else {
        std::cerr << "Error: " << err.message() << std::endl;
        sock.close();
    }
}

void ConHandler::handle_write(const boost::system::error_code& err, size_t bytes_transferred) {
    if (!err) {
        cout << "Server sent " << bytes_transferred << " bytes from thread " << gettid() << endl;
    }
    else {
        std::cerr << "Error: " << err.message() << std::endl;
        sock.close();
    }
}