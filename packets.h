//
// Created by ivar on 2021-05-12.
//

#ifndef SKYLORDS_CHAT_TEST_PACKETS_H
#define SKYLORDS_CHAT_TEST_PACKETS_H

#include <boost/uuid/uuid.hpp>
#include <unordered_map>

using std::string;

/*
 * Base class for all packets.
 */
class Packet {
public:
    /*
     * Serializes a packet to its string format.
     */
    virtual string encode() = 0;

    /*
     * De-serializes a packet into an unordered_map containing key-value pairs
     * of each entry.
     */
    static std::unordered_map<string, string> decode(string const &data);

    /*
     * Pads the given string with \0 to len.
     */
    static string pad_str(string str);

    enum { max_len = 1024 };
};

//TODO: Rewrite to use binary packaging
/*
 * Packet used for registering a user.
 */
class ConnPacket: Packet {
private:
    string username;
public:
    explicit ConnPacket(const string &name);

    string encode() override;
    string get_username();
};

/*
 * Packet used by clients sending messages to the server.
 */
class MsgInPacket: Packet {
private:
    string message;
    boost::uuids::uuid uuid;
public:
    MsgInPacket(const string &msg, const string &idstr);
    string encode() override;
    string get_message();
    boost::uuids::uuid get_uuid();
};

/*
 * Packet used by the server sending message to clients.
 */
class MsgOutPacket: Packet {
private:
    string user;
    string message;
public:
    MsgOutPacket(const string &user, const string &msg);
    string encode() override;
};

#endif //SKYLORDS_CHAT_TEST_PACKETS_H
