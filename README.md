##Skylords aptitude test server

###Build info
Project is build with CMake targeting >=C++20

###Running
Simply compile and run the server and client execs separately (run the server first). Number of threads can be
configured in the client's Main() function as "max_clients".

The current test case has every client register and send 1 message.

###Known areas for improvement
* Communication between ConHandler and Server should be encapsulated to some kind of middle-man
object or many-producer-single-consumer channel. This was not done to save time and because the current
  solution actually ended up being very readable and elegant for the scope of this project
  
* Lots of error-handling is yet to be implemented. I'm sure there are many fun ways to crash both server and
client if you know how to attack. This is also more a matter of time and return on effort for the scope of
  the assignment.
  
* Packets are not binary, but instead text. This was both due to time and the difficulties that come with
designing binary packets for unknown architectures (C++ has a lot of UB when it comes to serializing data like that).
  
As a more general note and as you've probably noticed reading through this, a lot of minor features and optimizations
were cut in the interest of "time". This is because I made an effort with this project to see how fast I could produce
something "good enough", and turns out it only took a few days! I hope you understand this approach and won't hold it against me. :)