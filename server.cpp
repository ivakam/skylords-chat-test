//
// Created by ivar on 2021-05-12.
//
#include "server.h"
#include <iostream>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/thread.hpp>

using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

void Server::start_accept() {
    ConHandler::pointer connection = ConHandler::create(io);

    acceptor_.async_accept(connection->socket(),
            [this, connection](boost::system::error_code err)
            {
                handle_accept(connection, err);
            });
}

void Server::handle_accept(const ConHandler::pointer& connection, const boost::system::error_code &err) {
    if (!err) {
        con_lock.lock();
        cons.push_back(connection);
        cout << "Adding connection " + std::to_string(cons.size()) + "..." << endl;
        connection->setup(shared_ptr<Server>(this));
        connection->start();
        con_lock.unlock();
    }
    start_accept();
}

void Server::register_user(ConHandler* conn, ConnPacket* conn_pkt) {
    string name = conn_pkt->get_username();
    cout << name << endl;
    boost::uuids::random_generator gen;
    boost::uuids::uuid id = gen();
    users[id] = name;
    auto idstr = boost::lexical_cast<std::string>(id);
    auto* msg = new MsgOutPacket("system", idstr);
    conn->send_message(msg);
    delete msg;
}

void Server::distribute_msg(MsgInPacket* msg_pkt) {
    // Drop message if user is not registered
    if (users.find(msg_pkt->get_uuid()) == users.end()) {
        return;
    }
    con_lock.lock();
    for (auto& c : cons) {
        auto* msg_out = new MsgOutPacket(users[msg_pkt->get_uuid()], msg_pkt->get_message());
        c->send_message(msg_out);
        delete msg_out;
    }
    con_lock.unlock();
}

int main() {
    const int POOL_SIZE = 8;

    try {
        io_context io_context;
        Server server(io_context);

        std::vector<boost::thread*> thread_pool;

        for (int i = 0; i < POOL_SIZE; i++) {
            thread_pool.push_back(new boost::thread([&io_context](){ io_context.run(); }));
        }

        for (auto& t : thread_pool) {
            t->join();
            delete t;
        }
    }
    catch (std::exception& e) {
        std::cerr << e.what() << endl;
    }
    return 0;
}
