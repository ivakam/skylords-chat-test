//
// Created by ivar on 2021-05-12.
//

#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/algorithm/string/split.hpp>
#include "packets.h"

std::unordered_map<string, string> Packet::decode(string const &data) {
    std::vector<string> data_v;
    std::unordered_map<string, string> parsed_data;
    // Split the data string on every \n and put each line into data
    boost::split(data_v, data, [](char c){return c == '\n';});
    // For every line, split the line at the ":" and store as key-value in parsed_data
    for (auto& s : data_v) {
        std::vector<std::string> temp;
        boost::split(temp, s, [](char c){return c == ':';});
        parsed_data[temp[0]] = temp[1];
    }
    return parsed_data;
}

string Packet::pad_str(string str) {
    int pad_len = max_len - str.size();
    if (pad_len > 0) {
        str.append(pad_len, '\0');
    }
    return str;
}

ConnPacket::ConnPacket(const string &name) {
    username = name;
}

string ConnPacket::encode() {
    return pad_str("type:conn\nusername:" + username);
}

string ConnPacket::get_username() {
    return username;
}

MsgInPacket::MsgInPacket(const string &msg, const string &idstr) {
    message = msg;
    auto id = boost::lexical_cast<boost::uuids::uuid>(idstr);
    uuid = id;
}

string MsgInPacket::encode() {
    auto uuidstr = boost::lexical_cast<string>(uuid);
    return pad_str("type:msgin\nmessage:" + message + "\n" + "uuid:" + uuidstr);
}

string MsgInPacket::get_message() {
    return message;
}

boost::uuids::uuid MsgInPacket::get_uuid() {
    return uuid;
}

MsgOutPacket::MsgOutPacket(const string &user, const string &msg) {
    this->user = user;
    message = msg;
}

string MsgOutPacket::encode() {
    return pad_str("type:msgout\nuser:" + user + "\nmessage:" + message);
}