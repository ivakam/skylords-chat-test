//
// Created by ivar on 2021-05-12.
//

#ifndef SKYLORDS_CHAT_TEST_CONHANDLER_H
#define SKYLORDS_CHAT_TEST_CONHANDLER_H

#include <string>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
class Server;
class MsgOutPacket;

using std::string;
using boost::asio::ip::tcp;
using boost::shared_ptr;
using boost::asio::mutable_buffer;

/*
 * Class responsible for managing/handling individual connections on the server side.
 * Holds a pointer to the shared server object in order to access some public functions.
 */
class ConHandler : public boost::enable_shared_from_this<ConHandler> {
private:
    tcp::socket sock;
    boost::asio::io_service::strand strand;
    shared_ptr<Server> server;
    enum { max_length = 1024 };
    char data[max_length];

    /*
     * Entrypoint for read loop. Uses read_some with handle_read as its handler.
     * This means that it does not wait until buffer is full and will read as much as possible every event loop.
     */
    void do_read();

    /*
     * Entrypoint for writing. The data to be written is padded with \0 to match the max_length.
     * Uses handle_write as its handler.
     */
    void do_write(string w_data);
public:
    typedef shared_ptr<ConHandler> pointer;
    explicit ConHandler(boost::asio::io_context& io)
        : sock(io),
          strand(io) {}

    /*
     * Wrapper function for getting the shared pointer.
     */
    static pointer create(boost::asio::io_context& io);

    /*
     * Getter for socket field.
     */
    tcp::socket& socket();

    /*
     * Setup method to be called after initializing the class. Sets the server field.
     */
    void setup(shared_ptr<Server> s);

    /*
     * Starts the read loop.
     */
    void start();

    /*
     * Read handler. Will parse incoming data and act accordingly, see implementation for details.
     */
    void handle_read(const boost::system::error_code& err, size_t bytes_transferred);

    /*
     * Write handler. Prints the amount of data sent and captures errors.
     */
    void handle_write(const boost::system::error_code& err, size_t bytes_transferred);

    /*
     * Encodes the message and sends it to do_write.
     */
    void send_message(MsgOutPacket* msg);
};

#endif //SKYLORDS_CHAT_TEST_CONHANDLER_H
