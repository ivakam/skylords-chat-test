//
// Created by ivar on 2021-05-12.
//

#ifndef SKYLORDS_CHAT_TEST_SERVER_H
#define SKYLORDS_CHAT_TEST_SERVER_H

#include "ConHandler.h"
#include "packets.h"
#include <boost/asio.hpp>
#include <boost/functional/hash.hpp>
#include <boost/coroutine/all.hpp>
#include <unordered_map>
#include <mutex>

using boost::asio::ip::tcp;
using std::string;
using std::mutex;

typedef std::unordered_map<boost::uuids::uuid, string, boost::hash<boost::uuids::uuid>> usermap;

class Server {
private:
    tcp::acceptor acceptor_;
    boost::asio::io_context& io;
    usermap users;
    mutex con_lock;
    std::vector<ConHandler::pointer> cons;
    void start_accept();
public:
    explicit Server(boost::asio::io_context& io_con)
    : acceptor_(io_con, tcp::endpoint(tcp::v4(), 1234)),
      io(io_con) {
        users = usermap();
        start_accept();
    }
    void handle_accept(const ConHandler::pointer& connection, const boost::system::error_code& err);
    void distribute_msg(MsgInPacket* msg_pkt);
    void register_user(ConHandler* conn, ConnPacket* conn_pkt);
};

#endif //SKYLORDS_CHAT_TEST_SERVER_H
