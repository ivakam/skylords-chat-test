//
// Created by ivar on 2021-05-12.
//

#include "client.h"
#include <iostream>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/exception/to_string.hpp>
#include "packets.h"

using boost::asio::ip::tcp;
using std::endl;
using std::cout;
using std::string;

void Client::do_read() {
    boost::asio::async_read(
            sock,
            // Read into buf
            boost::asio::buffer(buf, max_len),
            // Lambda for calling cont_read
            [this](boost::system::error_code err, std::size_t bytes_transferred)
            { cont_read(err, bytes_transferred); });
}

void Client::cont_read(const boost::system::error_code& err, size_t bytes_transferred) {
    try {
        if (err)
            throw boost::system::system_error(err);
        else {
            std::unordered_map<string, string>  parsed_data = Packet::decode(boost::to_string(buf));
            //TODO: Graceful handling of invalid data
            string user = parsed_data["user"];
            if (parsed_data["type"] != "msgout") {
                std::cerr << "Bad packet format" << std::endl;
                sock.close();
            }
            if (user == "system") {
                id = parsed_data["message"];
            }
            else {
                cout << user << " says: " << parsed_data["message"] << endl;
            }
            do_read();
        }
    }
    catch (std::exception & e) {
        std::cerr << e.what() << std::endl;
    }
}

void Client::run_test(std::string data) {
    try {
        sock.write_some(boost::asio::buffer(data, data.size()));
    }
    catch (std::exception & e) {
        std::cerr << e.what() << std::endl;
    }
}

void Client::run(c_args& args) {
    try {
        sock.connect(tcp::endpoint(boost::asio::ip::address::from_string(ip), port));
        do_read();
        boost::thread io_t = boost::thread(&Client::io_run, this);
        string tid = std::to_string(gettid());

        std::cout << "Running client on thread " << tid << std::endl;

        string test1 = ConnPacket("Billy" + tid).encode();
        run_test(test1);

        // Wait for response with id from server
        while (id.empty()) { }

        string test2 = MsgInPacket("Hello from thread " + tid, id).encode();
        run_test(test2);

        io_t.interrupt();
        io_t.join();
        sock.close();
    }
    catch (std::exception & e) {
        std::cerr << e.what() << std::endl;
    }
}

[[noreturn]] void Client::io_run() {
    for(;;) {
        io.run();
    }
}

int main() {
    string ip = "127.0.0.1";
    int port = 1234;
    int max_clients = 100;

    c_args args;
    args.ip = ip;
    args.port = port;

    std::vector<boost::thread*> threads;
    std::vector<Client*> clients;

    for (int i = 0; i < max_clients; i++) {
        clients.push_back(new Client(args));
        threads.push_back(new boost::thread(&Client::run, clients[i], args));
    }

    for (int i = 0; i < max_clients; i++) {
        threads[i]->join();
        delete threads[i];
    }
}
